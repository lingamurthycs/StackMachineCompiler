package com.hcl;

import java.util.NoSuchElementException;
import java.util.Scanner;
import java.util.Stack;

public class StackMachineCompiler {
	private Stack<Double> stack;

	public Number evaluate(String input) {
		try (Scanner scanner = new Scanner(input)) {
			// if empty string
			if (!scanner.hasNext())
				throw new IllegalArgumentException("Invalid expression.");

			try {
				// push the first operand to the stack
				stack.push(scanner.nextDouble());
				
				// if the scanner has another operand, push it
				// pop both operands and calculate the value with the operator in the scanner
				// push the calculated value
				while (scanner.hasNext()) {
					if (stack.size() == 1)
						stack.push(scanner.nextDouble());
					stack.push(operate(stack.pop(), stack.pop(), scanner.next()));
				}
			} catch (NoSuchElementException ne) {
				throw new IllegalArgumentException("Invalid expression");
			}
			// after calculations condition
			if (stack.size() != 1)
				throw new IllegalArgumentException("Invalid expression.");
		}

		// cosmetics
		Double d = stack.pop();
		if(d == d.intValue())
			return d.intValue();
		d *= 100;
		d = (double) Math.round(d);
		d /= 100;
		return d;
	}

	public StackMachineCompiler() {
		stack = new Stack<>();
	}

	private double operate(double operand1, double operand2, String operator) {
		switch (operator) {
		case "*":
			return operand2 * operand1;
		case "/":
			return operand2 / operand1;
		case "+":
			return operand2 + operand1;
		case "-":
			return operand2 - operand1;
		case "%":
			return operand2 % operand1;
		default:
			throw new IllegalArgumentException("Invalid operand");
		}
	}

}
