package com.hcl;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class TestStackMachineCompiler {
	private StackMachineCompiler compiler;

	@Before
	public void setup() {
		compiler = new StackMachineCompiler();
	}

	@Test
	public void testSingleNumberReturnsNumber() {
		assertEquals(50, compiler.evaluate("50"));
	}

	@Test(expected = IllegalArgumentException.class)
	public void testOnlyOperandThrowsException() {
		compiler.evaluate("*");
	}

	@Test(expected = IllegalArgumentException.class)
	public void testOperandsButNoOperator() {
		compiler.evaluate("* / -");
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testInvalidOperand() {
		compiler.evaluate("5 25 $");
	}

	@Test
	public void testCalculations() {
		assertEquals(105, compiler.evaluate("5 21 *"));
		assertEquals(33.33, compiler.evaluate("5 5 * 4 * 3 /"));
		assertEquals(35, compiler.evaluate("3 4 + 5 *"));
		assertEquals(-35, compiler.evaluate("3 4 + -5 *"));
		assertEquals(-52, compiler.evaluate("4 6 + -5.2 *"));
		assertEquals(1, compiler.evaluate("2 1 -"));
	}
}
